import { Component } from '@angular/core';

import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { NavController } from 'ionic-angular';

import { Storage } from '@ionic/storage';

import { Home } from '../home/home';

import { PasswordService } from '../../services/password.service';

import sjcl from 'sjcl';

@Component({
  selector: 'page-new_entry',
  templateUrl: 'new_entry.html'
})

export class NewEntry {
  entryForm: FormGroup;

  constructor(public navCtrl: NavController, builder: FormBuilder, public storage: Storage, private passwordService: PasswordService) {
    this.entryForm = builder.group({
      'text': [],
      'dateTime': [this.formatLocalDate()]
    })
  }

  submitEntry() {
    this.storage.get('entries').then((val) => {
      if (val == null) { val = []; }
      val.push({date: this.entryForm.controls['dateTime'].value, text: sjcl.encrypt(this.passwordService.Password(), this.entryForm.controls['text'].value)}); //implement password later
      var sorted = val.sort((a,b) => {
        return new Date(a.date) > new Date(b.date) ? -1 : new Date(a.date) < new Date(b.date) ? 1 : 0;
      });
      this.storage.set('entries', sorted);
    });
    this.navCtrl.setRoot(Home, {}, {
      animate: true,
      direction: 'back'
    });
  }

  formatLocalDate() {
    var now = new Date(),
        tzo = -now.getTimezoneOffset(),
        dif = tzo >= 0 ? '+' : '-',
        pad = function(num) {
            var norm = Math.abs(Math.floor(num));
            return (norm < 10 ? '0' : '') + norm;
        };
    return now.getFullYear()
        + '-' + pad(now.getMonth()+1)
        + '-' + pad(now.getDate())
        + 'T' + pad(now.getHours())
        + ':' + pad(now.getMinutes())
        + ':' + pad(now.getSeconds())
        + dif + pad(tzo / 60)
        + ':' + pad(tzo % 60);
  }
}
