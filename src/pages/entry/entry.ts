import { Component } from '@angular/core';

import { NavController, NavParams } from 'ionic-angular';

import { Storage } from '@ionic/storage';

import sjcl from 'sjcl';

@Component({
  selector: 'page-entry',
  templateUrl: 'entry.html'
})

export class Entry {
  data: any;
  date: Date;
  text: String;

  constructor(public navCtrl: NavController, public navParams: NavParams, private storage: Storage) {
    this.data = navParams.get('entry');

    this.date = new Date(this.data.date);
    this.text = this.data.text;
  }
}
