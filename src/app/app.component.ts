import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar, Splashscreen } from 'ionic-native';

import { Home } from '../pages/home/home';
import { Page2 } from '../pages/page2/page2';
import { NewEntry } from '../pages/new_entry/new_entry';
import { Setup } from '../pages/setup/setup';
import { Authenticate } from '../pages/authenticate/authenticate';

import { Storage } from '@ionic/storage';

@Component({
  templateUrl: 'app.html'
})

export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = Home;

  pages: Array<{title: string, component: any}>;

  constructor(public platform: Platform, public storage: Storage) {
    this.initializeApp();

    // used for an example of ngFor and navigation
    this.pages = [
      { title: 'Home', component: Home },
      { title: 'New Entry', component: NewEntry },
      { title: 'Entries', component: Page2 }
    ];
  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      StatusBar.styleDefault();
      Splashscreen.hide();

      this.platform.resume.subscribe(() => {
        this.nav.setRoot(Authenticate);
      });

      this.storage.get('password_is_set').then((val) => {
        if (val == null) {
          this.nav.setRoot(Setup);
        } else {
          this.nav.setRoot(Authenticate);
        }
      });
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }
}
